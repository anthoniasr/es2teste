export default class Validator {
    static validProduct(product) {
        const { description, buyPrice, sellPrice } = product;
        if (description.length < 3) {
            throw new Error('Descrição deve estar entre 3 e 50 caracteres@')
        }

        if (description.length > 51) {
            throw new Error('Descrição deve estar entre 3 e 50 caracteres@')
        }
        if (buyPrice > sellPrice) {
            throw new Error('Valor de venda não pode ser maior do que o valor de compra')
        }

        if (buyPrice < 0) {
            throw new Error('Valor de compra deve ser positivo')
        }

        if (sellPrice < 0) {
            throw new Error('Valor de venda deve ser positivo')
        }
            return product;

        }
    }



